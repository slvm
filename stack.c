/*
 * stack.c - SLVM stack manipulation
 *
 * Copyright (c) 2012 Peter Polacik <polacik.p@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* Config file */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Local includes */
#include "slvm.h"

/* System includes */
#include <stdio.h>
#include <stdlib.h>

/*
 * DATA STACK SECTION!
 */

/* Stack initialization */
int DS_init(SLVM_DS_stack_t *s, uint32_t max)
{
	s->st_max = max;
	s->st_count = 0;
	s->st_data = (uint8_t *)malloc(sizeof(uint8_t) * max);
	if (s->st_data == NULL)
		return 1;
	else
		return 0;
}

/* Stack destruction */
int DS_destroy(SLVM_DS_stack_t *s)
{
	s->st_max = 0;
	s->st_count = 0;
	free(s->st_data);
	return 0;
}

/* Stack - popping */
int DS_pop(SLVM_DS_stack_t *s, SLVM_register_t *loc)
{
	if (s->st_count > 0) {
		s->st_count--;
		*loc = s->st_data[s->st_count];
		return 0;
	} else return 1;
}

/* Stack - pushing */
int DS_push(SLVM_DS_stack_t *s, uint8_t val)
{
	if (s->st_count < s->st_max) {
		s->st_count++;
		s->st_data[s->st_count] = val;
		return 0;
	} else return 1;
}

/* Stack - accessing other items */
int DS_getelem(SLVM_DS_stack_t *s, SLVM_register_t *loc, uint32_t pos)
{
	if ((s->st_count > 0) && (pos < s->st_count)) {
		*loc = s->st_data[pos];
		return 0;
	} else return 1;
}

/* Stack - size */
int DS_size(SLVM_DS_stack_t *s)
{
	return s->st_count;
}

/*
 * CALL STACK SECTION!
 */

/* Stack init */
int CS_init(SLVM_CS_stack_t *s, uint32_t max)
{
	s->st_max = max;
	s->st_count = 0;
	s->st_data = (SLVM_context_t *)malloc(sizeof(SLVM_context_t) * max);
	if (s->st_data == NULL)
		return 1;
	else
		return 0;
}

/* Stack destruction */
int CS_destroy(SLVM_CS_stack_t *s)
{
	s->st_max = 0;
	s->st_count = 0;
	free(s->st_data);
	return 0;
}

/* Stack - popping */
int CS_pop(SLVM_CS_stack_t *s, SLVM_context_t *loc)
{
	if (s->st_count > 0) {
		s->st_count--;
		*loc = s->st_data[s->st_count];
		return 0;
	} else return 1;
}

/* Stack - push */
int CS_push(SLVM_CS_stack_t *s, SLVM_context_t val)
{
	if (s->st_count < s->st_max) {
		s->st_count++;
		s->st_data[s->st_count] = val;
		return 0;
	} else return 1;
}

/* Stack - getelem */
int CS_getelem(SLVM_CS_stack_t *s, SLVM_context_t *loc, uint32_t pos)
{
	if ((s->st_count > 0) && (pos < s->st_count)) {
		*loc = s->st_data[pos];
		return 0;
	} else return 1;
}

/* Stack - size */
int CS_size(SLVM_DS_stack_t *s)
{
	return s->st_count;
}
