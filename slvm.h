#ifndef _SLVM_H_
#define _SLVM_H_

/*
 * slvm.h - Main SLVM header file
 *
 * Copyright (c) 2012 Peter Polacik <polacik.p@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* Config file */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Local includes */

/* System includes */
#include <stdint.h>

/* FIXME: Just temporal so it compiles */
typedef char SLVM_context_t;

/*
 * Operation table
 * FIXME: Find something appropriate to be used as context type
 */
typedef int (*op_lod_t)(SLVM_context_t *, uint32_t *, int);

typedef struct {
    uint16_t op_code;
    uint8_t op_type;
    op_lod_t op_func;
    char *op_mnem;
} op_t;

/*
 * Data Stack
 *  Data are saved in bytes (uint8_t)
 *  Manipulation functions in stack.c!
 */
typedef struct {
    uint32_t st_count;
    uint32_t st_max;
    uint8_t *st_data;
} SLVM_DS_stack_t;

/*
 * Call Stack
 *  Data are saved in contexts
 *  Manipulation functions in stack.c!
 */
typedef struct {
    uint32_t st_count;
    uint32_t st_max;
    SLVM_context_t *st_data;
} SLVM_CS_stack_t;

#endif
