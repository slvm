#ifndef _SLE_H_
#define _SLE_H_

/*
 * sle.h - Definition of SLVM Executable File Format
 *
 * Copyright (c) 2012 Peter Polacik <polacik.p@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* Config file */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Local includes */
#include "slvm.h"

/* System includes */
#include <stdint.h>


/* FILE HEADER */


#define SLE_MAGIC_SZ    8 /* Size of SLE magic number */

/* SLE Magic Number */
#define SL_MAG0         0
#define SL_MAG1         1
#define SL_MAG2         2
#define SL_MAG3         3
#define SL_MAG4         4
#define SL_MAG5         5
#define SL_MAG6         6
#define SL_MAG7         7

#define SLEMAG0         0xff
#define SLEMAG1         'S'
#define SLEMAG2         'L'
#define SLEMAG3         'V'
#define SLEMAG4         'M'
#define SLEMAG5         '!'
#define SLEMAG6         'X'
#define SLEMAG7         'F'

/* Header Data - Class */
#define SLECLASSNONE    0x00
#define SLECLASSEXEC    0x01
#define SLECLASSLIB     0x02

/* Header Data - Byte Order */
#define SLEBONONE       0x00
#define SLEBOMSB        0x01
#define SLEBOLSB        0x02

/* Header Data - Wordsize */
#define SLEWORDSZNONE   0x00
#define SLEWORDSZ32BIT  0x01
#define SLEWORDSZ64BIT  0x02

/* Header Data - FP Encoding */
#define SLEFPNONE       0x00
#define SLEFPSINGLE     0x01
#define SLEFPDOUBLE     0x02


typedef struct {
    unsigned char s_magic[SLE_MAGIC_SZ]; /* magic number */
    uint8_t s_class; /* File class (0 unknown, 1 executable ...) */
    uint8_t s_byteorder; /* Byte order LSB/MSB (little/big endian) */
    uint8_t s_wordsize; /* Wordsize (4 - 32bit, 8 - 64bit) */
    uint8_t s_fp_encoding; /* C - float, double */
    uint8_t s_version_maj; /* File format version - major */
    uint8_t s_version_min; /* File format version - minor */
    uint16_t s_section_count; /* Number of sections in file */
} sle_hdr_content_t;

typedef struct {
    sle_hdr_content_t hdr_data;
    /* header size should ALWAYS be multiply of 16 */
    unsigned char s_padding[16 - (sizeof(sle_hdr_content_t) % 16)];
} sle_hdr_t;


/* SECTION TABLE */


/* Section Header - Types */
#define SETP_NULL       0x00
#define SETP_CODE       0x01
#define SETP_ICONST     0x02
#define SETP_FCONST     0x03
#define SETP_SCONST     0x04

typedef struct {
    uint8_t se_type;
    uint32_t se_offset;
    uint32_t se_size;
} sle_shdr_tbl_data_t;

typedef struct {
    sle_shdr_tbl_data_t shdr_data;
    unsigned char shdr_padding[16 - (sizeof(sle_shdr_tbl_data_t) % 16)];
} sle_shdr_tbl_t;


#endif /* _SLE_H_ */
